# README #

This is a basic I/O project made using various packages in python

### What is this repository for? ###

* contains the src code for this project

### How do I get set up? ###
* ***maybe create a virtual environment***
* pip3 install playsound
* pip3 install wikipedia 
* pip3 install matplotlib
* pip3 install tkinter
* pip3 install pyglet
* pip3 install webbrowser
 
### HOW to crete a virtual env? ###
- pip install virtualenv
- cd projectfolder #go to project folder
- virtualenv projectname #create the folder projectname 
- source projectname/bin/activate
 

### Who do I talk to? ###

* akshit.singh20@gmail.com
